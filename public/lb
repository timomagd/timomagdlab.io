#!/bin/bash
name="Tim Magdalinidis" #Name? [it's unused in the script]
webdir="$PWD" #blog directory
website="https://illegalfreedom.org" # Keep a trailing slash.
css="styles.css" #Location of CSS file
blogfile="stories.html" #Location of blog file
indexfile="blogindex.html" #Location of index file
rssfile="rss.xml" #Location of rss file
#Default editor
[ -z "$EDITOR" ] && EDITOR="vim"
#Installation
[ ! -d "$webdir/blog/.drafts" ] && #Checks if there is a .drafts folder?
read -erp "Initialize blog in $webdir?" ask && #And if not, it asks you if you want to make a blog in the webdir directory
	if [ "$ask" = "y" ]; then
	printf "Initializing blog system...\\n"
	mkdir -pv "$webdir/blog/.drafts" || printf "Error. Do you have write permissions in this directory?\\n" #Makes a hidden directory for the draft posts
	echo "Options +Indexes" > "$webdir/blog/.htaccess" #acts as a "database" file. `lb` stores filenames with their corresponding proper names and publishing dates there.
else
	exit
fi
#Function for creation of a post
newpost() { read -erp "Give a title for your post:
	" title #Title of the post
	echo "$title" | grep "\"" >/dev/null && printf "Double quotation marks (\") are not allowed in entry titles.\\n" && exit #Error message for "'s
	url="$(echo "$title" | iconv -cf UTF-8 -t ASCII//TRANSLIT | tr -d '[:punct:]' | tr '[:upper:]' '[:lower:]' | tr ' ' '-' | sed "s/-\+/-/g;s/\(^-\|-\$\)//g")"
#Creation of URL
	echo "AddDescription \"$title\" \"$url.html\"" >> "$webdir/blog/.htaccess" || { echo "Error: Is htaccess writeable?"; exit; } #Description(of what?)
	[ -f "$webdir/blog/.drafts/$url.html" ] && echo "There is already an existing draft entry of that same name/URL." && exit #Duplicate error message
	[ -f "$webdir/blog/$url.html" ] && echo "There is already an existing published entry of that same name/URL." && exit #Duplicate error message
	$EDITOR "$webdir/blog/.drafts/$url.html" ;} #Saves post in drafts
#Function to list post depending on the Option
listandReturn() { printf "Listing contents of %s.\\n" "$1" #What is "$1"???
	ls -rc "$1" | awk -F '/' '{print $NF}' | nl
	read -erp "Pick an entry by number to $2, or press ctrl-c to cancel. " number
	chosen="$(ls -rc "$1" | nl | grep -w  "$number" | awk '{print $2}')"
	basefile="$(basename "$chosen")" && base="${basefile%.*}" ;}
#Publish of a post
publish() { \
	delete
	htaccessentry=$(grep "$basefile" "$webdir/blog/.htaccess")
	realname="$(echo "$htaccessentry" | cut -d'"' -f2)"
	rssdate="$(grep "$basefile" blog/.htaccess | sed "s/.*\.html\"* *#*//g" | tr -d '\n')"
	[ -z "$rssdate" ] && rssdate="$(LC_TIME=en_US date '+%a, %d %b %Y %H:%M:%S %z')" # RSS date formats must comply with standards to validate.
	webdate="$(date '+%a, %d %b %Y %H:%M:%S %z')" # But this visible date you can set to any format.
	tmpdir=$(mktemp -d)
	#Creates the blog post
	printf "<html>\\n<head>\\n<title>%s</title>\\n<link rel='stylesheet' type='text/css' href='%s'>\\n<meta charset='utf-8'/>\\n</head>\\n<body>\\n<h1>%s</h1>\\n<small>[<a href='%s#%s'>link</a>&mdash;<a href='%s'>standalone</a>]</small>\\n%s\\n<footer>by <strong><a href='%s'>%s</a></strong></footer>\\n</body>\\n\\n</html>" "$realname" "$css" "$realname" "../$blogfile" "$base" "$basefile" "$(cat "$webdir/blog/.drafts/$basefile")" "$website" "$name" > "$webdir/blog/$basefile"
	#Updates the rss
	printf "\\n<item>\\n<title>%s</title>\\n<guid>%s%s#%s</guid>\\n<pubDate>%s</pubDate>\\n<description><![CDATA[\\n%s\\n]]></description>\\n</item>\\n\\n" "$realname" "$website" "$blogfile" "$base" "$rssdate" "$(cat "$webdir/blog/.drafts/$basefile")" >  "$tmpdir/rss"
	#tmpdir???
	printf "<div class='entry'>\\n<h2 id='%s'>%s</h2>\\n<small>[<a href='#%s'>link</a>&mdash;<a href='%s'>standalone</a>]</small>\\n%s\\n<small>%s</small>\\n</div>\\n" "$base" "$realname" "$base" "blog/$basefile" "$(cat "$webdir/blog/.drafts/$basefile")" "$webdate" > "$tmpdir/html"
	#Adds post in the index
	printf "<li>%s &ndash; <a href=\"blog/%s\">%s</a></li>\\n" "$(date '+%Y %b %d')" "$basefile" "$realname" > "$tmpdir/index"
	#Updates the blogfile, rss and index with the post. Adds the post after the "LB" in the original file
	sed -i "/<!-- LB -->/r $tmpdir/html" "$blogfile"
	sed -i "/<!-- LB -->/r $tmpdir/rss" "$rssfile"
	sed -i "/<!-- LB -->/r $tmpdir/index" "$indexfile"
	sed -i "/ \"$base.html\"/d" "$webdir/blog/.htaccess"
	echo "AddDescription \"$realname\" \"$basefile\" #$rssdate" >> "$webdir/blog/.htaccess"
	rm -f "$webdir/blog/.drafts/$chosen" #Deletes published post from draft
}
#Confirmation
confirm() { read -erp "Really $1 \"$base\"? (y/N) " choice && echo "$choice" | grep -i "^y$" >/dev/null || exit 1 ;}
#Deletion of a post
delete() { \
	sed -i "/<item/{:a;N;/<\\/item>/!ba};/#$base<\\/guid/d" $rssfile
	sed -i "/<div class='entry'>/{:a;N;/<\\/div>/!ba};/id='$base'/d" $blogfile
	sed -i "/<li>.*<a href=\"blog\\/$base.html\">/d" $indexfile
	rm "$webdir/blog/$basefile" 2>/dev/null && printf "Old blog entry removed.\\n" ;}
#Choose a post and revise it. It then gets saved in the drafts
revise() { awk '/^<small>\[/{flag=1;next}/<footer>/{flag=0}flag' "$webdir/blog/$chosen" > "$webdir/blog/.drafts/$basefile"
	"$EDITOR" "$webdir/blog/.drafts/$basefile"
	printf "Revision stored in blog/.drafts. Publish as normal entry when desired.\\n" ;}
#Options
case "$1" in
	n*) newpost ;; #Creation of a new post option
	e*) listandReturn "$webdir"/blog/.drafts/ edit && "$EDITOR" "$webdir/blog/.drafts/$chosen" ;; #Edit draft option
	p*) listandReturn "$webdir"/blog/.drafts/ publish && publish ;; #Publish post option
	t*) listandReturn "$webdir"/blog/.drafts/ trash && confirm trash && rm -f "$webdir/blog/.drafts/$chosen" && sed -i "/ \"$base.html\"/d" "$webdir/blog/.htaccess" ; printf "Draft deleted.\\n" ;; #Delete draft option
	d*) listandReturn "$webdir"/blog/ delete && confirm delete && delete && sed -i "/ \"$base.html\"/d" "$webdir/blog/.htaccess" ;; #Delete posted entry
	r*) listandReturn "$webdir"/blog/ revise && revise ;; #Edit posted entry
	*) printf "lb blog system by Luke Smith <luke@lukesmith.xyz>\\nUsage:\\n  lb n:\\tnew draft\\n  lb e:\\tedit draft\\n  lb p:\\tpublish/finalize draft\\n  lb r:\\trevise published entry\\n  lb t:\\tdiscard draft\\n  lb d:\\tdelete published entry\\n\\nBe sure to have the following pattern added to your RSS feed, blog file and blog index:\\n\\n<!-- LB -->\\n\\nNew content will be added directly below that sequence. This is required.\\nSee https://github.com/LukeSmithxyz/lb for more.\\n" ;; #Menu screen
esac
